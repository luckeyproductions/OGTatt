#!/bin/sh

cd `dirname $0`;
if [ ! -d Dry ]
then
    git clone https://gitlab.com/luckeyproductions/Dry
    cd Dry 
else
    cd Dry
    git checkout master
    git pull
fi

./cmake_clean.sh
./cmake_generic.sh . \
    -DDRY_ANGELSCRIPT=0 -DDRY_2D=0 \
    -DDRY_SAMPLES=0 -DDRY_TOOLS=0
make
cd ..
