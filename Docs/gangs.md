### Tourists

![](../Resources/Textures/Decals/Tourists.png)

Just enjoying their holiday in a country they love... no really!  
After creating a new character this is your default alignment. During character creation emblems of the gangs show how the appearance you pick effects your ability to join each of them. When leaving a gang you become a tourist again and the respect you gained is turned into a wanted level for that gang.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
None | Camera, Shades, Shorts, Hawaii blouse, Straw hat | Join gang | None | None | None

-----------------------

### Chaosquad
 
![](../Resources/Textures/Decals/Chaosquad.png)

Everything is magic if you look at it that way.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Anywhere | Witches hat, Robe | Cannot be bothered when someone leaves | None | All | Random

-----------------------

### Association of Autonomous Astronauts

![](../Resources/Textures/Decals/AAA.png)

Trying to get away from this place these nerds ran into a little budget problem. Nevertheless they have gained some experience with rockets along the way. Often they need parts that only Zebratools provides at frustratingly high prices.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Pingrad (HQ), Balkan, Grunstatt and Airstrip 1 | Space helmet, Backpack, Glasses | Jet pack and nitro | | Rat | ϕ 42,-

-----------------------

### Obscurinati

![](../Resources/Textures/Decals/Obscurinati.png)

Having spent too much time in the dark these suits were cast back down to earth from their space thrones to fight for their life like any other mere mortal. Their archenemies are the Luciferians as Art is too small for _two_ forces of darkness.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Zombuntu (HQ, triangular "pentagon"), Airstrip 1, Agrobar, Ichell | Baseball cap | Stealth (pink = transparency), Jamming |  | Ox | ϕ 555,-

-----------------------

### Pantherae Byssii

![](../Resources/Textures/Decals/BeigePanthers.png)

The bastard child of white supremacy and the black panther movement. These "mixed-race" individuals have had it with not being acknowledged as a race of their own resulting in the extremist _neo genus_ doctrine. Shortly after their inception the rest of the world realized the absurdity of racism.


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Agrobar (HQ), Zombunto, Noopolis, Balkan |  |  | Midtone skin | Tiger | ϕ 55,-

-----------------------

### Confusians

![](../Resources/Textures/Decals/Confusians.png)

Hail Eris! Whatever that means. Their disorganized approach helped these Discordians in claiming the position of biggest religion. The SubGenii constantly trying to come up with news ways to change this.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Monkong (HQ), Grunstatt,  Pingrad, Sloompur |  | Apple (single health boost) |  | Rabbit | ϕ 23,-

-----------------------

### Project Rainbow

![](../Resources/Textures/Decals/Hippies.png)

Peace or die!  
They blame the Lagoonies for the fact their music doesn't bring in any money.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Axllotus (HQ), Grunstatt, Noopolis, Ånslo |   | Guerrilla gardening | None | Dragon | It's free, man

-----------------------
 
### Police Force Now!
![](../Resources/Textures/Decals/Police.png)

Spotting targets is so much easier when you're dressed in blue. And these people know it. Also recruiting new goons works better with a catchy name.  
They get money every time a bomb goes off.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
Anywhere but Noopolis, HQ in Airstrip 1 | Police cap, Shades | Siren in any vehicle | Never been in a gang | Snake | ϕ 10,-

-----------------------

### Zebratools Inc.

![](../Resources/Textures/Decals/Zebratools.png)

Power tools, pharmaceuticals and fashion. They loathe the AAA for threatening their business and try to keep them small. Startled by cops and understanding towards Pantherae Byssii.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
Sloompur (HQ), Monkong, Zombuntu, Noopolis | Fedora | Perma-member, perma-death, kick and LOIC | Experience | Horse | ϕ 100,-

-----------------------

### Luciferians
    
![](../Resources/Textures/Decals/Luciferians.png)

Driven by demoney forces - aka the _almighty buck_ - and bodily pleasures under the motto "Be evil" and believing in blood magic and the power of buck dancing. They are constantly trying to prove that there is no greater evil on Art, Obscurinati included. The central decision making organ is referred to as the Goat See.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Ichell (HQ), Ånslo, Agrobar, Balkan | Pendant | Knife (lethal punch), Armored cars | Wealth | Goat | ϕ 666,-

 Rank | Title
------|-------
 1    | Young buck
 2    | Ram rider
 3    | Horned one

-----------------------

#### Untied Morons o' Murky

![](../Resources/Textures/Decals/UMoM.png)

A (white) Christian biker gang, all that's left of a sunken empire. Stupidity is their motto, blindness their creed and think they're better then most people because Moron starts with "more". They consider the Pantherae Byssii to be a living nightmare from their dark past.


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Noopolis (HQ), Airstrip 1, Zombuntu | Bandanna, Shades | Bullet resistant | Fat | Monkey | ϕ 10,-

-----------------------

#### SubGenii

![](../Resources/Textures/Decals/SubGenii.png)

Realizing they'll need to make some more noise if they want to reclaim the title of biggest religion, that's exactly what they'll do.


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
 Balkan (HQ), Noopolis, Grunstatt| Pipe | Tracking and sharing data | No pink | Rooster | ϕ 1,-

-----------------------

#### Lagoonies


![](../Resources/Textures/Decals/Lagoonies.png)


All these people became criminals overnight for sharing audio and decided to stop running forcing them to break many other laws as well.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
Ånslo (HQ), Grunstatt, Noopolis | Eyepatch, Bicorne, Tricorne, Bandana, Visor | Tracking and sharing data |  | Dog | ϕ 5,-

-----------------------

#### Bio-Front
![](../Resources/Textures/Decals/BioFront.png)

Fighting pollution, freeing animals and slaughtering meat eaters. They despise police and are in contact with beings from the Vega system.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Grunstatt (HQ), Noopolis, Axllotus, Balkan | Crowbar, Barricades | Saboteurs | Not too fat | Pig | ϕ 34,-

-----------------------

#### X-Terminators
![](../Resources/Textures/Decals/XTerminators.png)

These black budget androids from the recent past just got an unholy update from future Martian prankster ghosts that originated from planet X. It makes these machines highly annoyed by all stuff that moves. Being unable to turn off their cameras, some have "lost" an arm. They hate the AAA to their cores.

Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
None, admins and bots only | Shades | Run fast and flip cars | Mechanical | Cat | ϕ1000,-
