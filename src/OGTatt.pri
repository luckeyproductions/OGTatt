SOURCES += \
    $$PWD/bike.cpp \
    $$PWD/bullet.cpp \
    $$PWD/car.cpp \
    $$PWD/citizen.cpp \
    $$PWD/cookiejar.cpp \
    $$PWD/deco.cpp \
    $$PWD/effect.cpp \
    $$PWD/explosion.cpp \
    $$PWD/firepit.cpp \
    $$PWD/grass.cpp \
    $$PWD/hitfx.cpp \
    $$PWD/honti.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/controllable.cpp \
    $$PWD/blockmap/level.cpp \
    $$PWD/blockmap/heightprofile.cpp \
    $$PWD/blockmap/tile.cpp \
    $$PWD/blockmap/cyberplasm/cyberplasm.cpp \
    $$PWD/blockmap/cyberplasm/witch.cpp \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/muzzle.cpp \
    $$PWD/ogtattcam.cpp \
    $$PWD/pickup.cpp \
    $$PWD/player.cpp \
    $$PWD/spawnmaster.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/streetlight.cpp \
#    $$PWD/tile.cpp \
    $$PWD/vehicle.cpp \
    $$PWD/wallcollider.cpp \
    $$PWD/character.cpp \
    $$PWD/frop.cpp \
    $$PWD/animatedbillboardset.cpp

HEADERS += \
    $$PWD/bike.h \
    $$PWD/bullet.h \
    $$PWD/car.h \
    $$PWD/citizen.h \
    $$PWD/cookiejar.h \
    $$PWD/deco.h \
    $$PWD/effect.h \
    $$PWD/explosion.h \
    $$PWD/firepit.h \
    $$PWD/grass.h \
    $$PWD/hitfx.h \
    $$PWD/honti.h \
    $$PWD/inputmaster.h \
    $$PWD/controllable.h \
    $$PWD/blockmap/level.h \
    $$PWD/blockmap/heightprofile.h \
    $$PWD/blockmap/tile.h \
    $$PWD/blockmap/cyberplasm/cyberplasm.h \
    $$PWD/blockmap/cyberplasm/witch.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/muzzle.h \
    $$PWD/ogtattcam.h \
    $$PWD/pickup.h \
    $$PWD/player.h \
    $$PWD/spawnmaster.h \
    $$PWD/sceneobject.h \
    $$PWD/streetlight.h \
#    $$PWD/tile.h \
    $$PWD/vehicle.h \
    $$PWD/wallcollider.h \
    $$PWD/character.h \
    $$PWD/frop.h \
    $$PWD/animatedbillboardset.h
