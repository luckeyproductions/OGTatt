/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HEIGHTPROFILE_H
#define HEIGHTPROFILE_H

#include "../luckey.h"

struct HeightProfile
{
    struct Elevation: public PODVector<int>
    {
        using PODVector::PODVector;

        String toString() const
        {
            String elevationString{};

            for (int e: *this)
                elevationString.Append(String{ e } + " ");

            return elevationString.Trimmed();
        }
    };

    struct Normals: public PODVector<IntVector2>
    {
        Normals(): PODVector<IntVector2>()
        {
            Resize(4u, IntVector2::ZERO);
        }

        void set(int index, const IntVector2& normal)
        {
            IntVector2& intNormal{ operator[](index) };
            intNormal = normal;

            while (intNormal.x_ < -7)
                intNormal.x_ += 16;
            while (intNormal.x_ > 8)
                intNormal.x_ -= 16;

            while (intNormal.y_ < -7)
                intNormal.y_ += 16;
            while (intNormal.y_ > 8)
                intNormal.y_ -= 16;
        }

        Vector3 normal(int index) const
        {
            return Quaternion{ 22.5f * At(index).x_, Vector3::BACK } *
                   Quaternion{ 22.5f * At(index).y_, Vector3::RIGHT } *
                   Vector3::UP;
        }

        String toString() const
        {
            String normalsString{};

            for (IntVector2 n: *this)
                normalsString.Append(n.ToString() + ";");

            return normalsString.Substring(0, normalsString.Length() - 1u);
        }
    };
/*
    0,1 B   C 1,1
          X
    0,0 A   D 1,0
*/
    static IntVector3 cornerOffset(char index)
    {
        IntVector3 offset{ 0, 0, 0 };

        switch (index) {
        default: break;
        case 1: ++offset.z_; break;
        case 2: ++offset.x_; ++offset.z_; break;
        case 3: ++offset.x_; break;
        }

        return offset;
    }

    HeightProfile(): HeightProfile(0) {}
    HeightProfile(int h): HeightProfile(h, h, h, h) {}

    HeightProfile(const Elevation& profile):
        elevation_{ profile },
        normals_{},
        span_{ IntVector2::ZERO, IntVector2::ONE }
    {
    }

    HeightProfile(int a, int b, int c, int d):
        elevation_{ { a, b, c, d } },
        normals_{},
        span_{ IntVector2::ZERO, IntVector2::ONE }
    {
    }

    static HeightProfile fromString(const String& elevation, const String& normals = "", const String& span = "")
    {
        HeightProfile res{};
        const StringVector splitE{ elevation.Split(' ')};
        for (unsigned i{ 0u }; i < Min(splitE.Size(), res.elevation().Size()); ++i)
            res.set(i, ToInt(splitE.At(i)));

        if (!normals.IsEmpty())
        {
            const StringVector splitN{ normals.Split(';')};
            for (unsigned i{ 0u }; i < Min(splitN.Size(), res.normals().Size()); ++i)
                res.setNormalInt(i, ToIntVector2(splitN.At(i)));
        }

        if (!span.IsEmpty())
            res.setSpan(ToIntRect(span));

        return res;
    }

//    unsigned ToHash() const
//    {
//        unsigned hash = 37;
//        hash = 37 * hash + elevation_[0];
//        hash = 37 * hash + elevation_[1];
//        hash = 37 * hash + elevation_[2];
//        hash = 37 * hash + elevation_[3];

//        return hash;
//    }

    int at(unsigned index) const
    {
        assert(index < 4u);
        return elevation_.At(index);
    }

    void set(unsigned index, int height)
    {
        assert(index < 4u);
        elevation_.At(index) = height;
    }

    Vector3 normal(unsigned index) const
    {
        return normals_.normal(index);
    }

    void setNormalInt(unsigned index, const IntVector2& normal)
    {
        normals_.At(index) = normal;
    }

    const IntRect& span() const { return span_; }

    void setSpan(const IntRect& span)
    {
        span_ = span;
    }

//    int Reduce();

    int max() const
    {
        int res{ elevation_.At(0) };
        for (unsigned i{ 1 }; i < elevation_.Size(); ++i)
        {
            if (elevation_.At(i) > res)
                res = elevation_.At(i);
        }
        return res;
    }

    int min() const
    {
        int res{ elevation_.At(0) };
        for (unsigned i{ 1 }; i < elevation_.Size(); ++i)
        {
            if (elevation_.At(i) < res)
                res = elevation_.At(i);
        }
        return res;
    }

    const Elevation& elevation() const { return elevation_; }
    const Normals& normals() const   { return normals_; }

    bool operator ==(const HeightProfile& rhs) const
    {
        return rhs.elevation() == elevation_ &&
               rhs.normals() == normals_ &&
               rhs.span_ == span_;
    }

    bool operator !=(const HeightProfile& rhs) const
    {
        return rhs.elevation() != elevation_ ||
               rhs.normals() != normals_ ||
               rhs.span_ != span_;
    }

    bool isFlat(bool onlyNormals = false) const
    {
        for (const IntVector2& n: normals_)
            if (n != IntVector2::ZERO)
                return false;

        if (onlyNormals)
            return true;
        else
            return min() == max();
    }

    IntVector2 size() const { return span_.Size(); }
    Vector3 offCenter() const
    {
        const Vector2 flatCenter{ (span_.Min() + span_.Max() - IntVector2::ONE) * .5f };

        return { flatCenter.x_, 0.f, flatCenter.y_ };
    }

    static const IntRect DEFAULTSPAN;

private:
    Elevation elevation_;
    Normals normals_;
    IntRect span_;
};

#endif // HEIGHTPROFILE_H
