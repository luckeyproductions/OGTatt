/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "level.h"
#include "cyberplasm/cyberplasm.h"

#include "tile.h"

Tile::Tile(Context* context): Component(context),
    tileModel_{ nullptr },
    profile_{}
{
}

void Tile::Initialize(const HeightProfile& profile, Material* material)
{
    profile_ = profile;
    generateTile();
    tileModel_->SetMaterial(material);

        collider_ = node_->GetParent()->CreateComponent<CollisionShape>();
        collider_->SetMargin(.025f);

        if (!profile_.isFlat())
            collider_->SetTriangleMesh(tileModel_->GetModel(), 0, Vector3::ONE, Vector3::DOWN * collider_->GetMargin() + node_->GetPosition(), node_->GetRotation());
        else
            collider_->SetBox({ 1.f * profile_.size().x_, 1.f, 1.f * profile_.size().y_ }, node_->GetPosition() + Vector3::UP * (profile_.min() - 1));
}

void Tile::OnNodeSet(Node *node)
{
    if (!node)
        return;

    tileModel_ = node_->CreateComponent<StaticModel>();
}

void Tile::generateTile()
{
    Level* blockMap{ node_->GetParent()->GetComponent<Level>() };

    if (!blockMap)
        return;

    const Vector3 blockSize{ blockMap->GetBlockSize() };
    const float blockHeight{ blockSize.y_ };
    const IntVector2 size{ profile_.size() };
    const Vector3 offCenter{ profile_.offCenter() * blockMap->GetBlockSize() };
    const Vector3 halfSize{ blockSize.x_ * size.x_ * .5f, blockHeight * .5f, blockSize.z_ * size.y_ * .5f };
    const bool flat{ profile_.isFlat() };

    Witch::Spell rect{ {}, Witch::Spell::QUAD };

    for (unsigned c{ 0u }; c < 4u; ++c)
    {
        const int e{ profile_.elevation().At(c) };
        const IntVector3 offset{ HeightProfile::cornerOffset(c) * 2 - IntVector3{ 1, 0, 1 } };
        const Vector3 pos{ halfSize * offset + Vector3::UP * (e * blockHeight - halfSize.y_) + offCenter };
        const Vector3 normal{ profile_.normal(c) };

        rect.Push(Witch::Rune{ { pos }, { normal.Length() == 0.f ? pos.Normalized() : normal } });
    }

    Cyberplasm* cyberplasm{ GetSubsystem<Cyberplasm>() };
    SharedPtr<Model> tileModel{ context_->CreateObject<Model>() };

    if (flat)
    {
        tileModel = cyberplasm->Summon({ rect }, 0);
    }
    else
    {
        tileModel->SetNumGeometries(1);
        tileModel->SetBoundingBox({ Vector3{ -halfSize.x_, profile_.min() * blockHeight - halfSize.y_, -halfSize.z_ } + offCenter,
                                    Vector3{  halfSize.x_, profile_.max() * blockHeight - halfSize.y_,  halfSize.z_ } + offCenter }); /// Span offset

        const int lods{ 3 };
        tileModel->SetNumGeometryLodLevels(0, lods);

        for (int lod{ 0 }; lod < lods; ++lod)
        {
            const int r{ Max(1, 1 << (lods - lod - 1)) };
            tileModel->SetGeometry(0, lod, cyberplasm->Conjure(rect.Cast({ r * size.x_ - 1, r * size.y_ - 1})));
            tileModel->GetGeometry(0, lod)->SetLodDistance(lod * 23.5f);
        }
    }

    tileModel_->SetModel(tileModel);
}

