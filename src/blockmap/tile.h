/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TILE_H
#define TILE_H

#include "heightprofile.h"

class Tile: public Component
{
    DRY_OBJECT(Tile, Component);

public:
    Tile(Context* context);

    void Initialize(const HeightProfile& profile, Material* material = nullptr);

protected:
    void OnNodeSet(Node* node) override;

private:
    void generateTile();

    StaticModel* tileModel_;
    CollisionShape* collider_;
    HeightProfile profile_;
};

#endif // TILE_H
