/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BULLET_H
#define BULLET_H

#include "sceneobject.h"

class Bullet: public SceneObject
{
    friend class Player;
    DRY_OBJECT(Bullet, SceneObject);

public:
    static void RegisterObject(Context *context);
    Bullet(Context* context4);

    void Start() override;
    virtual void Set(const Vector3& position, const Vector3& direction, Node* owner = nullptr);
    void Update(float timeStep) override;

protected:
    SharedPtr<RigidBody> rigidBody_;
    SharedPtr<StaticModel> model_;

private:
    Node* owner_;
    float age_;
    float timeSinceHit_;
    float lifetime_;
    bool fading_;
    float damage_;
    void HitCheck(float timeStep);
    void Disable();
};

#endif
