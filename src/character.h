/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NPC_H
#define NPC_H

#include "controllable.h"

class Vehicle;

enum Hair{HAIR_BALD, HAIR_SHORT, HAIR_MOHAWK, HAIR_SEAGULL, HAIR_MUSTAIN, HAIR_FROTOAD, HAIR_FLATTOP, HAIR_ALL};
enum Actions{RUN, JUMP, ENTER, BURP};

class Character: public Controllable
{
    DRY_OBJECT(Character, Controllable);

public:
    static void RegisterObject(Context *context);
    Character(Context* context);

    void Start() override;
    void Set(const Vector3& position) override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void UpdateKinematics(StringHash eventType, VariantMap& eventData);

    float GetHealth(){return health_;}
    void Hit(float damage);
    void SetObjective(Node* node) { objective_ = node; aim_ = Vector3::ZERO; }
    Substance GetSubstance([[maybe_unused]] const Vector3& position = Vector3::ZERO) const override;

    void Think(float timeStep) override;
    void HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData);

    bool IsAlive() const { return node_->HasComponent<RigidBody>(); }

protected:
    AnimatedModel* hairModel_;

    AnimationController* animCtrl_;
    bool male_;
    Hair hairStyle_;
    Vector<Color> colors_;

    float maxHealth_;
    float health_;

    float shotInterval_  = Random(2) ? 0.1f + Random(2) * 0.7f : 0.0f; /// Random Pistol/Uzi/Unarmed
    float sinceLastShot_ = 0.0f;
    Sound* shot_sfx;

    float sinceLastTurn_;
    float turnInterval_;
    void EnterVehicle(Vehicle* vehicle);
    void EnterNearestVehicle();

private:
    Node* objective_;
    Vector3 target_;

    void CreateBody();
    void CreateRagdoll();
    void Die();
    void CreateRagdollBone(const String &boneName, ShapeType type, const Vector3 &size, const Vector3 &position, const Quaternion &rotation);
    void CreateRagdollConstraint(const String &boneName, const String &parentName, ConstraintType type, const Vector3 &axis, const Vector3 &parentAxis, const Vector2 &highLimit, const Vector2 &lowLimit, bool disableCollision);

    void HandleAction(int actionId);

};

#endif // NPC_H
