/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HEMP_H
#define HEMP_H

#include "sceneobject.h"

class Frop: public SceneObject
{
    DRY_OBJECT(Frop, SceneObject);

public:
    static void RegisterObject(Context *context);
    Frop(Context* context);

    void Start() override;
    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);

private:
    virtual void Update(float timeStep);
    StaticModel* fropModel_;
    Vector3 scale_;

    float growthStart_;

    float age_;
};

#endif // HEMP_H
