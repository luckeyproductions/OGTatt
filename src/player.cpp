/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "player.h"

#include "inputmaster.h"

Player::Player(int playerId, Context* context): Object(context),
    playerId_{ playerId },
//    autoPilot_{ playerId_ == 2 && !GetSubsystem<Input>()->GetJoystickByIndex(playerId_-1) },
    autoPilot_{ false },
//    autoPilot_{ true },
    alive_{ true },
    balance_{ 0u },
    wallet_{ 0u },
    interest_{ -0.05f }
{
//    Node* guiNode{ MC->scene_->CreateChild("GUI3D") };
//    gui3d_ = guiNode->CreateComponent<GUI3D>();
//    gui3d_->Initialize(playerId_);
}

void Player::Die()
{
    alive_ = false;
}
void Player::Respawn()
{
//    ClearInventory();
    alive_ = true;
}

void Player::SetBalance(int bucks)
{
    balance_ = bucks;
//    gui_->SetBalance(bucks);
}

void Player::SetWallet(unsigned bucks)
{
    wallet_ = bucks;
//    gui_->SetWallet(bucks);
}

void Player::Pay(int receive, bool cash)
{
    if (cash)
        wallet_ += receive;
    else
        SetBalance(balance_ + receive);
}

void Player::EnterLobby()
{
}

void Player::EnterPlay()
{
}

Vector3 Player::GetPosition()
{
    return GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_)->GetNode()->GetPosition();
}

Controllable* Player::GetControllable()
{
    Controllable* controllable{ GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_) };

    if (controllable)
        return controllable;
    else
        return nullptr;
}
