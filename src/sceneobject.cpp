/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "sceneobject.h"

SceneObject::SceneObject(Context *context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::Set(const Vector3& position)
{
    node_->SetPosition(position);
    node_->SetEnabledRecursive(true);
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

void SceneObject::PlaySample(Sound* sample, float gain)
{
    if (!sample)
        return;

    SoundSource3D* sampleSource{ node_->CreateComponent<SoundSource3D>(LOCAL) };
    sampleSource->SetDistanceAttenuation(23.0f, 88.0f, 0.5f);
    sampleSource->SetGain(gain);
    sampleSource->SetSoundType(SOUND_EFFECT);
    sampleSource->SetAutoRemoveMode(REMOVE_COMPONENT);

    sampleSource->Play(sample);
}

bool SceneObject::IsSilent() const
{
    return !GetNode()->HasComponent<SoundSource3D>();

//    for (SoundSource* s: GetComponents<SoundSource3D>(Sources_)
//    {
//        if (s->IsPlaying())
//            return false;
//    }

//    return true;
}
